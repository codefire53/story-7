from django.test import TestCase,LiveServerTestCase,Client
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
import chromedriver_binary
# Create your tests here.
class PageTest(TestCase):
    def test_index_exist(self):
        response=Client().get('/')
        self.assertEqual(response.status_code,200)
    def test_page_error(self):
        response=Client().get('/error/')
        self.assertEqual(response.status_code,404)
    def test_index_template(self):
        response=Client().get('/')
        self.assertTemplateUsed(response,'index.html')
class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        #self.selenium=webdriver.Chrome()

    def tearDown(self):
        self.selenium.quit()

    def test_theme_button(self):        
        selenium=self.selenium  
        selenium.get(self.live_server_url)
        button=selenium.find_element_by_tag_name('button')
        header=selenium.find_element_by_class_name("accordion-header")
        content=selenium.find_element_by_class_name("accordion-content")
        background=selenium.find_element_by_tag_name('body')
        self.assertEqual(background.value_of_css_property('background-color'),'rgba(255, 255, 255, 1)')
        self.assertEqual(header.value_of_css_property('background-color'),'rgba(18, 175, 146, 1)')
        self.assertEqual(content.value_of_css_property('color'),'rgba(0, 0, 0, 1)')
        button.click()
        time.sleep(5)
        header=selenium.find_element_by_class_name("accordion-header")
        content=selenium.find_element_by_class_name("accordion-content")
        self.assertEqual(background.value_of_css_property('background-color'),'rgba(0, 0, 0, 1)')
        self.assertEqual(header.value_of_css_property('background-color'),'rgba(172, 172, 172, 1)')
        self.assertEqual(content.value_of_css_property('color'),'rgba(255, 255, 255, 1)')

    def test_accordion(self):
        self.selenium.get(self.live_server_url)
        content = self.selenium.find_element_by_class_name("accordion-content")
        display = content.value_of_css_property("display")
        self.assertEqual("none", display)
        self.selenium.find_element_by_class_name("accordion-header").click()
        time.sleep(3)
        content = self.selenium.find_element_by_class_name("accordion-content")
        display = content.value_of_css_property("display")
        self.assertEqual("block", display)
        self.selenium.find_element_by_class_name("accordion-header").click()
        time.sleep(3)
        content = self.selenium.find_element_by_class_name("accordion-content")
        display = content.value_of_css_property("display")
        self.assertEqual("none", display)










