
 $('.accordion-header').click( function() {
        $(this).next().slideToggle();
})

function changeBackground(){
    var body=$('body');
    var button=$('button');
    var accordion=$('.accordion-header')
    var back_accordion=$('.accordion')
    var content=$('.accordion-content')
    var title=$('#title')
    if(button.attr('id')=="white"){
        body.css("background-color","rgb(0,0,0)");
        accordion.css("background-color","rgb(172,172,172)");
        button.attr("id","black");
        back_accordion.css("background-color","rgb(0,0,0)");
        content.css("color","rgb(255,255,255)");
        title.css("color","rgb(255,255,255)");
    }
    else{
        body.css("background-color","rgb(255,255,255)");
        accordion.css("background-color","rgb(18, 175, 146)");
        button.attr("id","white");
        back_accordion.css("background-color","rgb(255,255,255)");
        content.css("color","rgb(0,0,0)");
        title.css("color","rgb(0,0,0)");
    }
    return false;
}